"""Functionality to process and check webxdc asset files. """

from zipfile import ZIP_DEFLATED, ZipFile

import toml


def check_xdc_consistency(xdc_path, source_def):
    """Check .xdc file, iteratively yielding any found problems.

    Args:
        xdc_path: path to the .xdc file.
        source_def: the SourceDef instance of an app.
    """
    MAX_SUMMARY_CHARS = 30
    MIN_DETAILS_CHARS = 40

    # See SUBMIT.md in repository root for the restrictions implemented below
    # If you modify the below code ensure that SUBMIT.md reflects it.

    with ZipFile(xdc_path) as zipfile:
        manifest, icon = parse_manifest_and_icon(zipfile)

    manifest_sc = manifest.get("source_code_url")
    if manifest_sc:
        sc = source_def.source_code_url
        if sc != manifest_sc:
            yield f"warn: manifest {manifest_sc} != {sc}"
    description = source_def.description
    if not description:
        yield "error: 'description' field not in xdcget.ini"
    else:
        lines = description.strip().split("\n")
        if len(lines[0]) > MAX_SUMMARY_CHARS:
            extra = len(lines[0]) - MAX_SUMMARY_CHARS
            yield f"error: description summary {extra} chars too much"
        if lines[0][-1:] == ".":
            yield "error: description summary ends with '.'"
        if len(lines) < 2:
            yield "error: description misses detail lines"
        else:
            joint = " ".join(lines[1:])
            if len(joint) < MIN_DETAILS_CHARS:
                yield "error: description details have less than 40 chars"

    if "app_id" in manifest:
        yield f"error: manifest has app_id {manifest['app_id']}, ignoring"
    if "version" in manifest:
        yield "error: manifest has 'version', ignoring"
    if not icon:
        yield "error: no 'icon.png' or 'icon.jpg' in .xdc file"


def parse_manifest_and_icon(zipfile):
    """Return parsed manifest.toml and icon byte content
    from the passed in zipfile."""
    iconbytes = manifest = None
    for fname in zipfile.namelist():
        if fname.lower().endswith("manifest.toml"):
            content = zipfile.read(fname)
            manifest = toml.loads(content.decode("utf-8"))
        elif fname.lower() in ("icon.png", "icon.jpg"):
            iconbytes = zipfile.read(fname)

    return manifest, iconbytes


def export_for_xdcstore(index_entry, cache_dir, export_dir):
    """Return a tuple of two path (.xdc file and icon file).

    This function takes a index_entry's '.xdc' file from the cache dir
    and writes out two export files into the export_dir:

    - the ".xdc" file, result of post-processing the entry's cache_dir file
    - an ".jpg" or ".png" file containing the app's icon
    """
    cache_path = cache_dir.joinpath(index_entry.cache_relname)
    export_path = export_dir.joinpath(index_entry.cache_relname)

    new_zf_path = export_path.parent.joinpath(export_path.name + ".tmp")
    # note that the original asset might use a different compression
    # method but we will write it out with standard ZIP_DEFLATED
    # because that's the only method that Delta Chat core supports as of 1.117.0
    zf = ZipFile(cache_path)
    new_zf = ZipFile(new_zf_path.open("wb"), compression=ZIP_DEFLATED, mode="w")
    icon_path = None
    for fname in zf.namelist():
        content = zf.read(fname)
        if fname == "manifest.toml":
            manifest = toml.loads(content.decode("utf-8"))
            # we know exactly where we were getting the source asset from
            # so it's a better source than what was manually specified
            manifest["source_code_url"] = index_entry.source_code_url
            # put the "tag_name" into the produced manifest so that
            # together with the source_code_url we have a somewhat precise
            # reference for any ".xdc" file the store hands out
            manifest["tag_name"] = index_entry.tag_name
            content = toml.dumps(manifest).encode("utf-8")
        if fname.lower() in ("icon.jpg", "icon.png"):
            icon_path = export_dir.joinpath(f"{index_entry.app_id}-{fname}")
            icon_path.write_bytes(content)

        new_zf.writestr(fname, data=content)
    new_zf.close()
    new_zf_path.rename(export_path)
    return [export_path, icon_path]
