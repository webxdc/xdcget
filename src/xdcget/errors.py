""" Exception objects used in other modules. """


class MissingAPI(Exception):
    """Required API section in configuration is missing."""

    def __init__(self, app_id):
        super().__init__(
            f"Configuration error: app section {app_id!r}"
            "is missing a matching `api:*` section"
        )
        self.app_id = app_id


class MissingSection(Exception):
    """Required section in configuration is missing."""

    def __init__(self, section):
        super().__init__(f"Configuration error: section {section!r} is required")
        self.section = section


class EmptyOrUnsetEnvVar(Exception):
    """Environment variable set in configuration is unset or empty."""

    def __init__(self, section, var):
        super().__init__(
            f"Configuration error in section {section}: environment variable {var!r} is not set or empty"  # noqa
        )
        self.section = section
        self.var = var


class IncompleteCredentials(Exception):
    """Incomplete API access credentials."""

    def __init__(self, section, provided, missing):
        super().__init__(
            f"Configuration error in section {section}: field {provided!r} present but field {missing!r} is missing"  # noqa
        )
        self.section = section
        self.provided_field = provided
        self.missing_field = missing


class TokenError(Exception):
    """Codeberg returns 403 sometimes when a token is used too often."""

    def __init__(self, request):
        super().__init__(f"403 HTTP Error during fetching {request.url}")
        self.request = request
        self.url = request.url


class InvalidAppId(Exception):
    """Invalid app-id for a app."""


class NoReleases(Exception):
    """The source does not provide any releases."""

    def __init__(self, url):
        self.url = url
