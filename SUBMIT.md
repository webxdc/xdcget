
# Submitting a webxdc app for xdcget's default list of apps 

Within the "xdcget" repository we maintain the [xdcget.ini file](./xdcget.ini)
containing a maintained, curated set of working webxdc apps, including 3rd party apps. 
Our base configuration is [used by public chat bots](https://delta.chat/en/2023-08-11-xstore) which offer webxdc apps to users. If you submit an entry for your app, 
all such bots will make it available to their end-users. 

# Steps to submit a webxdc app 

- Create a tagged release on your own development repository and
  attach a `*.xdc` asset file to your release.  The attached `*.xdc`
  file must include a `manifest.toml` and icon, either `icon.png` or
  `icon.jpg`.  See [the format specification](https://webxdc.org/docs/spec/format.html) 
  for details.

- To submit your app metadata, you need to have a 
  [Codeberg account](https://codeberg.org/).

  Once logged into Codeberg, [fork the xdcget repository](https://codeberg.org/repo/fork/123200),
  and create a new app metadata section in `xdcget.ini` 
  and then submit a Pull Request with your added app metadata section. 

This is the example structure of all existing webxdc app sections: 

    [app:mywebxdc]
    category = tool
    source_code_url = https://codeberg.org/my/webxdc
    description = short_summary
        one or more lines saying more about the app
        including platform restrictions.


The following restrictions and recommendations apply to app sections: 

- A section name MUST be named like `[app:NAME]` where `NAME` is your unique app id
  which MUST only contain lowercase alphanumeric characters and use `-` as separator. 

- A section MUST contain a `category` field which MUST be set to `tool` or `game`

- A section MUST contain a `source_code_url` field specifying the link
  to a public Codeberg or Github repository which in turn offers tagged
  releases offering an ".xdc" release asset file.
  Usually, this is the same URL as set in [`manifest.toml`](https://webxdc.org/docs/spec/format.html#the-manifesttoml-file).
  If your app is ported/modified from another source
  or you want to give credits otherwise,
  the README at the `source_code_url` is a good place for that.

- A section MUST contain a multi-line `description` field.

  The first line is the "short summary" which MUST NOT exceed 30 characters
  and MUST NOT have a `.` at the end. 

  The second (and more) lines need to be indented and contain 1-2 sentences 
  describing additional info (take inspiration from other entries) but note: 

  - don't specifically mention that your app can be shared in a chat
    as this is true for all apps processed through xdcget. 

  - mention any caveats (or platform restrictions) in usage. 
