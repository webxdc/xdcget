
# xdcget: submission system for webxdc app stores 

Please see [Submit a webxdc app for inclusion in default stores](./SUBMIT.md),
or read on if you are interested in contributing to development 
or running your own webxdc app store. 

![xdcget logo](./assets/logo-256x256.png)

This respository contains the `xdcget` command line tool 
which uses the root-level [xdcget.ini](xdcget.ini) configuration file
in order to:

- collect git-released webxdc apps from configured sources 

- export them to the [xdcstore bot](https://codeberg.org/webxdc/store)
  which in turn [can be live contacted](https://delta.chat/en/2023-08-11-xstore) 
  to search and share latest available apps in messenger chats. 


## Helping with development / Getting Started with xdcget 

0. Clone or fork this repository, make sure you have a basic Python development setup. 

1. Install `xdcget` command line tool in "editable" mode from a local checkout:

        pip install -e . 

2. Edit `xdcget.ini`, credentials are not mandatory but it is recommended
   that you set environment variables containing your credentials for Codeberg/Github API usage
   to avoid quickly reaching rate limits.
   See below for how to get API access tokens from these repository/release providers. 

3. Run the `update` command to retrieve newest webxdc app releases 
   for repositories listed in `xdcget.ini` and export them to the `export` directory: 

        xdcget update 


### Running automated tests and linters 

[tox](https://pypi.org/project/tox) is used for running tests and linters:

    tox 

which in turn installs and uses: 

- [black](https://pypi.org/project/black) for formatting, 
- [ruff](https://pypi.org/project/ruff) for linter,  
- [pytest](https://pypi.org/project/pytest) for running offline and online tests. 

If you want to run the online tests,
you need to set some environment variables
and run:

```
export XDCGET_CODEBERG_USER=<your-codeberg-user-name>
export XDCGET_CODEBERG_TOKEN=<paste-your-codeberg-access-token-here>
export XDCGET_GITHUB_USER=<your-github-user-name>
export XDCGET_GITHUB_TOKEN=<paste-your-github-access-token-here>
pytest --online   # or 
tox -- --online
```

**IMPORTANT:** Pull Requests with new features / bug fixes should come with automated tests.


### Building and publishing xdcget releases

Typically deployments are based on the "main" branch of the 'xdcget' repository. 
From time to time we publish the "xdcget" command line tool to pypi. 
Checklist for xdcget maintainers: 

- `pip install tox build twine` to install development dependencies

- `tox` to run tests

- `python -m build` to build the distribution files

- [get API-tokens from PyPI (only pypi maintainer can do it)](https://pypi.org/manage/account/)
  to be able to upload to PyPI repositories. 

- Use git to tag a release before uploading (e.g. "git tag" and "git push --tags") 
  The version of xdcget (also obtained via `xdcget --version`) is dynamically 
  computed using [setuptools-scm](https://pypi.org/project/setuptools-scm/)

- `twine upload dist/*` to upload all built distribution files 


### Getting a Codeberg API access token 

Login with Codeberg and open https://codeberg.org/user/settings/applications to generate a new token.
Select "Repository and Organization Access" = "Public only" and set permission "repository" to "Read"
to grant the token access to `https://codeberg.org/api/v1/repos/{owner}/{repo}/releases/latest` URLs.
You can copy the resulting API token into your clipboard
and then set it into the environment variables you declared in the config file:

    # bash example
    export XDCGET_CODEBERG_USER=<your-codeberg-user-name>
    export XDCGET_CODEBERG_TOKEN=<paste-your-codeberg-access-token-here>

### Getting a Github API access token 

Login with github and open https://github.com/settings/tokens
to generate a new token.  This token does not need any access
to your private repos -- it's only used for querying releases 
of public repositories.  You may give it 90 days or other expiration
times as you feel fine with. 
You can copy the resulting API token into your clipboard
and then set it into the environment variables you declared in the config file:

    # bash example
    export XDCGET_GITHUB_USER=<your-github-user-name>
    export XDCGET_GITHUB_TOKEN=<paste-your-github-access-token-here>

