from pathlib import Path

import pytest

from xdcget.config import InvalidAppId, read_config, validate_app_id, write_xdcget_ini
from xdcget.errors import EmptyOrUnsetEnvVar, IncompleteCredentials, MissingAPI
from xdcget.main import main


class TestConfig:
    def test_init_and_read_config_basics(self, tmpdir, monkeypatch):
        # set some fake users and tokens to avoid configuration errors in read_config()
        for api in ["GITHUB", "CODEBERG"]:
            monkeypatch.setenv(f"XDCGET_{api}_USER", "test")
            monkeypatch.setenv(f"XDCGET_{api}_TOKEN", "test")

        tmpdir.chdir()
        main(["init"])
        config = read_config(Path("xdcget.ini"))
        assert config.export_dir == Path("export_dir").absolute()
        assert config.cache_dir == Path("cache_dir").absolute()
        cb = config.api_defs[0]
        assert cb.root_url == "https://codeberg.org"
        assert cb.api_url == "https://codeberg.org/api/v1/"
        gh = config.api_defs[1]
        assert gh.root_url == "https://github.com"
        assert gh.api_url == "https://api.github.com/"

    def test_omitted_credentials(self, tmpdir, monkeypatch):
        tmpdir.chdir()
        common_kwargs = dict(
            path=Path("xdcget.ini"),
            export_dir=Path("export_dir"),
            cache_dir=Path("cache_dir"),
        )

        # auth fields are optional, it is OK if they are omitted
        write_xdcget_ini(
            **common_kwargs,
            codeberg_user_env_var="",
            codeberg_token_env_var="",
            github_user_env_var="",
            github_token_env_var="",
        )
        config = read_config(common_kwargs["path"])
        for api_def in config.api_defs:
            assert api_def.auth is None

    def test_incomplete_credentials(self, tmpdir, monkeypatch):
        tmpdir.chdir()
        common_kwargs = dict(
            path=Path("xdcget.ini"),
            export_dir=Path("export_dir"),
            cache_dir=Path("cache_dir"),
        )

        # codeberg's token_env_var is set but user_env_var is not set
        write_xdcget_ini(
            **common_kwargs,
            codeberg_user_env_var="",
        )
        with pytest.raises(IncompleteCredentials):
            read_config(common_kwargs["path"])

    def test_read_config(self, tmpdir, monkeypatch):
        tmpdir.chdir()
        common_kwargs = dict(
            path=Path("xdcget.ini"),
            export_dir=Path("export_dir"),
            cache_dir=Path("cache_dir"),
        )

        # auth env vars are set but to invalid/unset env vars
        write_xdcget_ini(
            **common_kwargs,
            codeberg_user_env_var="INVALID_CODEBERG_USER",
            codeberg_token_env_var="INVALID_CODEBERG_TOKEN",
            github_user_env_var="INVALID_GITHUB_USER",
            github_token_env_var="INVALID_GITHUB_TOKEN",
        )
        with pytest.raises(EmptyOrUnsetEnvVar):
            read_config(common_kwargs["path"])

        # set the unset env vars, then it should succeed
        for api in ["github", "codeberg"]:
            monkeypatch.setenv(f"invalid_{api}_user".upper(), "test")
            monkeypatch.setenv(f"invalid_{api}_token".upper(), "test")
        config = read_config(common_kwargs["path"])
        for api_def in config.api_defs:
            assert api_def.auth is not None

    def test_list_apps(self, config_example1):
        (source1, api1), (source2, api2) = config_example1.list_apps()
        assert source1.source_code_url == "https://github.com/webxdc/checklist"
        assert source1.app_id == "webxdc-checklist"
        assert api1.auth
        assert (
            api1.get_latest_release_url(source1)
            == "https://api.github.com/repos/webxdc/checklist/releases/latest"
        )
        assert source2.source_code_url == "https://codeberg.org/webxdc/poll"
        assert source2.app_id == "webxdc-poll"
        assert api2.auth
        assert (
            api2.get_latest_release_url(source2)
            == "https://codeberg.org/api/v1/repos/webxdc/poll/releases/latest"
        )

    def test_list_unmatched_app_source(self, iniconfig):
        iniconfig.add_source(
            app_id="not-found",
            source_code_url="https://notconfigured.org/webxdc/checklist",
        )
        config = iniconfig.create()
        with pytest.raises(MissingAPI) as excinfo:
            config.list_apps()
        assert excinfo.value.app_id == "not-found"

    @pytest.mark.parametrize("app_id", ["A-b", "a-b-", "-a-b", "0a", "Aa", "a.b"])
    def test_app_id_fails(self, app_id):
        with pytest.raises(InvalidAppId):
            validate_app_id(app_id)

    @pytest.mark.parametrize("app_id", ["a", "a-b", "a-b-c", "a0", "a0-b"])
    def test_app_id_ok(self, app_id):
        validate_app_id(app_id)
