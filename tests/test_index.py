import pytest
import requests

from xdcget.config import SourceDef
from xdcget.errors import NoReleases
from xdcget.index import Index, IndexEntry, query_latest_release_asset


class TestIndex:
    @pytest.fixture
    def pseudo_Config(self):
        class Config:
            def __init__(self, path, app_ids=[]):
                self.index_path = path
                self.app_ids = app_ids
                self.source_defs = []

            def get_app(self, app_id):
                return (SourceDef("hello", "", "tool", ""), None)

        return Config

    @pytest.fixture
    def pseudo_IndexEntry(self):
        releases = []

        def maker(app_id):
            name = app_id.split("-")[1]
            tag_name = f"0.{len(releases)}.0"
            url = "https://example.org/asset.xdc"
            releases.append(1)
            return IndexEntry(
                app_id=app_id,
                tag_name=tag_name,
                url=url,
                date="",
                source_code_url="",
                description="",
                name=name,
                category="tool",
                size=42,
            )

        return maker

    def test_write_one(self, tmp_path, pseudo_Config, pseudo_IndexEntry):
        path = tmp_path.joinpath("test.lock")

        config = pseudo_Config(path)

        index = Index(config)
        ir = pseudo_IndexEntry("webxdc-hello")
        index.set_entry(ir)
        index.persist_if_modified()

        assert index.get_entry(app_id="xzy") is None
        release = index.get_entry(app_id="webxdc-hello")
        assert release.url == ir.url
        assert release.tag_name == "0.0.0"
        assert release.name == "hello"
        assert release.cache_relname == "webxdc-hello-0.0.0.xdc"
        assert release == index.get_entry(app_id="webxdc-hello")

        index.set_entry(release)
        release2 = index.get_entry("webxdc-hello")
        assert release2.cache_relname == release.cache_relname
        assert release2.tag_name == release.tag_name

    def test_write_three(self, tmp_path, pseudo_Config, pseudo_IndexEntry):
        path = tmp_path.joinpath("test.lock")
        config = pseudo_Config(path)
        index = Index(config)
        for num in range(4):
            ir = pseudo_IndexEntry("webxdc-hello")
            index.set_entry(ir)
        index.persist_if_modified()

        rel = index.get_entry("webxdc-hello")
        assert rel.tag_name == "0.3.0"
        assert rel.url == ir.url
        assert rel.size == 42

        rel2 = index.get_entry("webxdc-hello")
        assert rel2 == rel


def test_query_latest_release_asset(config_example1):
    (source1, api1), (source2, api2) = config_example1.list_apps()
    session = requests.Session()
    tag_name, url, date = query_latest_release_asset(session, source1, api1)
    assert tag_name
    assert "checklist.xdc" in url
    assert date

    tag_name, url, date = query_latest_release_asset(session, source2, api2)
    assert tag_name
    assert "poll.xdc" in url
    assert date


def test_query_latest_release_no_assets(iniconfig, requests_mocker):
    iniconfig.add_source(
        app_id="webxdc-checklist",
        source_code_url="https://github.com/webxdc/checklist",
    )
    config = iniconfig.create()

    # simulate no-releases
    requests_mocker.requests_mock.get(
        "https://api.github.com/repos/webxdc/checklist/releases/latest",
        json={"tag_name": "v1.0.1", "assets": []},
    )
    source1, api1 = config.get_app("webxdc-checklist")

    with pytest.raises(NoReleases):
        query_latest_release_asset(requests.Session(), source1, api1)
