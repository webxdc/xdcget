from xdcget.asset import ZipFile, export_for_xdcstore, parse_manifest_and_icon


def test_parse_manifest_and_icon(testdata, zipfactory):
    iconbytes = testdata.read_bytes("icon.png")
    example_entry = testdata.newIndexEntry()
    zf = ZipFile(zipfactory.byteio(example_entry, icon=iconbytes))
    manifest, icon = parse_manifest_and_icon(zf)
    assert icon == iconbytes
    assert manifest["name"] == example_entry.name
    assert manifest["source_code_url"] == example_entry.source_code_url


def test_export_for_xdcstore(testdata, zipfactory, exportdir):
    iconbytes = testdata.read_bytes("icon.png")
    entry = testdata.newIndexEntry()
    cache_dir = zipfactory.path(entry, icon=iconbytes).parent
    export_dir = exportdir.joinpath(entry.cache_relname).parent
    export_path = export_dir.joinpath(f"{entry.app_id}-{entry.tag_name}.xdc")
    assert not export_path.exists()
    written = export_for_xdcstore(entry, cache_dir, export_dir)
    assert len(written) == 2
    export_path2, icon_path = written
    assert export_path == export_path2
    assert icon_path.name == f"{entry.app_id}-icon.png"
